Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mysql-connector-python
Source: http://dev.mysql.com/downloads/connector/python/

Files: *
Copyright: Copyright (c) 2009-2024, Oracle and/or its affiliates. All rights reserved.
           Use is subject to license terms.
License: GPL-2 (with OpenSSL and FOSS License Exception)
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License, version 2.0, as
 published by the Free Software Foundation.
 .
 This program is also distributed with certain software (including
 but not limited to OpenSSL) that is licensed under separate terms,
 as designated in a particular file or component or in included license
 documentation.  The authors of MySQL hereby grant you an
 additional permission to link the program and your derivative works
 with the separately licensed software that they have included with
 MySQL.
 .
 Without limiting anything contained in the foregoing, this file,
 which is part of MySQL Connector/Python, is also subject to the
 Universal FOSS Exception, version 1.0, a copy of which can be found at
 http://oss.oracle.com/licenses/universal-foss-exception.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License, version 2.0, for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License v2 can be found in "/usr/share/common-licenses/GPL-2".
Comment:
 MySQL Connector/Python is licensed under the terms of the GPLv2
 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>, like most
 MySQL Connectors. There are special exceptions to the terms and
 conditions of the GPLv2 as it is applied to this software, see the
 FLOSS License Exception
 <http://www.mysql.com/about/legal/licensing/foss-exception.html>.


Files: debian/*
Copyright: 2011-2022 Sandro Tosi <morph@debian.org>
           2024 Debian Python Team <team+python@tracker.debian.org>
License: GPL-2
 On Debian systems, the complete text of the GNU General Public
 License v2 can be found in "/usr/share/common-licenses/GPL-2".
